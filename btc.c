#include "btc.h"
#include "btc_internal.h"

void
btc_get(char *btc)
{
	CURL *curl;
	curl = curl_easy_init();
	curl_easy_setopt(
	  curl, CURLOPT_URL, "https://api.cryptonator.com/api/ticker/BTC-AUD");
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, btc_save);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, btc);
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

	int res = curl_easy_perform(curl);
	if (res != CURLE_OK)
		fprintf(stderr, "curl err");
	curl_easy_cleanup(curl);
}

void
btc_save(char *ptr, size_t size, size_t nmemb, void *stream)
{
	fprintf(stderr, "%.9s", &ptr[48]);
	snprintf(stream, 9, "%s", &ptr[48]);
}
