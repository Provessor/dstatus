#include "volume.h"

void
vol_get(unsigned long *vol, wchar_t *volIcon)
{
	int sset;
	long set, min, max;
	snd_mixer_t *handle;
	snd_mixer_selem_id_t *sid;

	snd_mixer_open(&handle, 0);
	snd_mixer_attach(handle, "pulse");
	snd_mixer_selem_register(handle, NULL, NULL);
	snd_mixer_load(handle);

	snd_mixer_selem_id_alloca(&sid);
	snd_mixer_selem_id_set_index(sid, 0);
	snd_mixer_selem_id_set_name(sid, "Master");
	snd_mixer_elem_t *elem = snd_mixer_find_selem(handle, sid);

	snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
	snd_mixer_selem_get_playback_volume(elem, SND_MIXER_SCHN_MONO, &set);
	*vol = (set - min) * 100 / (max - min);

	snd_mixer_selem_get_playback_switch(elem, SND_MIXER_SCHN_MONO, &sset);
	*volIcon = sset ? L'' : L'';

	snd_mixer_close(handle);
}
