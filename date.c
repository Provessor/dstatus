#include "date.h"

void
date_get(char *date)
{
	time_t t;
	struct tm tm;

	t = time(NULL);
	tm = *localtime(&t);
	strftime(date, 18, "%a %d %b - %H%M", &tm);
}
