#include "battery.h"
#include "btc.h"
#include "config.h"
#include "date.h"
#include "volume.h"
#include "wifi.h"
#include "mail.h"

#include <errno.h>
#include <locale.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <wchar.h>
#include <xcb/xcb.h>
#include <xcb/xcb_atom.h>

xcb_connection_t *conn;
xcb_screen_t *scr;
xcb_window_t root;

unsigned short bat, wifi;
unsigned int mail;
unsigned long vol;
char date[18], btc[8];
wchar_t batIcon, volIcon, wifiIcon;

void
set_root_name(void);

void *
loop_read(void *vargp);
void *
loop_short(void *vargp);
void *
loop_medium(void *vargp);
void *
loop_long(void *vargp);

int
main(int argc, char **argv)
{
	const char *display_name = NULL;
	conn = xcb_connect(NULL, NULL);
	scr = xcb_setup_roots_iterator(xcb_get_setup(conn)).data;
	root = scr->root;

	setlocale(LC_CTYPE, "");

	date_get(date);
	bat_get(&bat, &batIcon);
	vol_get(&vol, &volIcon);
	wifi_get(&wifi, &wifiIcon);
	mail_get(&mail);
	set_root_name();
	/* btc_get(btc); */

	FILE *f;
	f = fopen("/tmp/dstatus.fifo", "w+");
	fclose(f);

	pthread_t loopShort;
	pthread_create(&loopShort, NULL, loop_short, NULL);

	pthread_t loopMedium;
	pthread_create(&loopMedium, NULL, loop_medium, NULL);

	pthread_t loopLong;
	pthread_create(&loopLong, NULL, loop_long, NULL);

	loop_read(NULL);

	pthread_join(loopShort, NULL);
	pthread_join(loopMedium, NULL);
	pthread_join(loopLong, NULL);
}

void
set_root_name(void)
{
	wchar_t status[120];
	/* swprintf(status, 120, L"  %lc %d\% |  %s AUD |  %s ", batIcon, bat,
	 * btc, date); */
	swprintf(status,
	         120,
	         L"   %d | %lc %d\% | %lc %ld\% | %lc %d\% |  %s ",
	         mail,
	         wifiIcon,
	         wifi,
	         volIcon,
	         vol,
	         batIcon,
	         bat,
	         date);
	char statusNorm[120];
	wcstombs(statusNorm, status, 120);
	xcb_change_property(conn,
	                    XCB_PROP_MODE_REPLACE,
	                    root,
	                    XCB_ATOM_WM_NAME,
	                    XCB_ATOM_STRING,
	                    8,
	                    strlen(statusNorm),
	                    statusNorm);
	xcb_flush(conn);
}

void *
loop_read(void *vargp)
{
	while (1) {
		int wd, fd;
		char buf[1];
		FILE *f;

		fd = inotify_init();
		wd = inotify_add_watch(fd, "/tmp/dstatus.fifo", IN_CLOSE_WRITE);

		read(fd, buf, 1);

		f = fopen("/tmp/dstatus.fifo", "r");
		switch (fgetc(f))
		{
		case 'a':
			vol_get(&vol, &volIcon);
			break;
		}
		fclose(f);

		set_root_name();

		inotify_rm_watch(fd, wd);
		close(fd);
	}
}

void *
loop_short(void *vargp)
{
	while (1) {
		sleep(SLEEP_TIME_SHORT);
		bat_get(&bat, &batIcon);
		date_get(date);
		set_root_name();
	}
}

void *
loop_medium(void *vargp)
{
	while (1) {
		sleep(SLEEP_TIME_MEDIUM);
		wifi_get(&wifi, &wifiIcon);
	}
}

void *
loop_long(void *vargp)
{
	while (1) {
		sleep(SLEEP_TIME_LONG);
		/* mail_sync(); */
		mail_get(&mail);
		/* btc_get(btc); */
	}
}
