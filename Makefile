include config.mk

SRC = main.c date.c battery.c volume.c wifi.c btc.c mail.c
OBJ = ${SRC:.c=.o}

all: options dstatus

options:
	@echo dstatus build options:
	@echo "FLAGS = ${FLAGS}"
	@echo "LIBS  = ${LIBS}"
	@echo "CC    = ${CC}"

.c.o:
	${CC} -c ${FLAGS} $<

${OBJ}: config.mk

dstatus: ${OBJ}
	${CC} -o $@ -Ofast ${OBJ} ${LIBS}

clean:
	rm -f dstatus ${OBJ}

install:
	cp -f dstatus /usr/bin/

uninstall:
	rm -f /usr/bin/dstatus


.PHONY: all options
