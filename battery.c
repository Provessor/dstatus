#include "battery.h"

void
bat_get(unsigned short *bat, wchar_t *batIcon)
{
	FILE *f;
	char ch[4];

	f = fopen("/sys/class/power_supply/BAT0/capacity", "r");
	fgets(ch, 4, f);
	*bat = atoi(ch);
	fclose(f);

	f = fopen("/sys/class/power_supply/AC/online", "r");

	if (fgetc(f) == '1')
		*batIcon = L''; /*  */
	else if (*bat >= 90)
		*batIcon = L'';
	else if (*bat >= 65)
		*batIcon = L'';
	else if (*bat >= 35)
		*batIcon = L'';
	else if (*bat >= 10)
		*batIcon = L'';
	else
		*batIcon = L'';

	fclose(f);
}
