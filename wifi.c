#include "wifi.h"

void
wifi_get(unsigned short *wifi, wchar_t *wifiIcon)
{
	FILE *f;
	int wifiRaw;
	char wifiC[82];

	f = fopen("/sys/class/net/wlan0/operstate", "r");
	fgets(wifiC, 82, f);
	fclose(f);

	if (strncmp(wifiC, "up", 2) != 0) {
		*wifiIcon = L'';
		*wifi = 0;
	} else {
		*wifiIcon = L'';

		f = fopen("/proc/net/wireless", "r");
		fgets(wifiC, 82, f);
		fgets(wifiC, 82, f);
		fgets(wifiC, 82, f);
		fclose(f);

		wifiRaw = (wifiC[15] - '0') * 10 + wifiC[16] - '0';
		*wifi = (short)((float)wifiRaw / 70 * 100);
	}
}
