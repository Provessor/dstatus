void
obsd_vol_init(void)
{
	FILE *fdPause;
	struct audio_status status;
	FILE *fdVol;
	mixer_ctrl_t *values;

	fdPause = fopen("/dev/audioctl0", "r");
	if (ioctl(fileno(fdPause), AUDIO_GETSTATUS, &status) < 0)
		return;

	fdVol = fopen("/dev/mixer", "r");
	if (ioctl(fileno(fdVol), AUDIO_MIXER_READ, &values) < 0)
		fprintf(stderr, "oops");

	vol_get(status, values);

	fclose(fdPause);
}

void *
obsd_vol_loop_start(void *vargp)
{
	return 0;
}

void
obsd_vol_get(struct audio_status status, mixer_ctrl_t *values)
{
	if (status.pause == 1)
		volIcon = L'';
	else
		volIcon = L'';

	fprintf(stderr, "ok %d ok\n", values->un.value.level[0]);
}
